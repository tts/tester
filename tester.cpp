#include <math.h>
#include <getopt.h>
#include <signal.h>
#include <sys/file.h>
#include <linux/input.h>
#include <gtkmm.h>

int monitornum = 0, monnum = 0;
Glib::RefPtr<Gdk::Display> displayRef;
Glib::RefPtr<Gdk::Screen> screen;
Gtk::ApplicationWindow *mainwin;

bool main_screen = false;

class GuiTester : public Gtk::DrawingArea {
public:
    GuiTester(Gtk::Main *papp);

protected:
    Gtk::Main *app;
    gdouble curr_x, curr_y;
    bool first_time;
    bool on_draw(const Cairo::RefPtr<Cairo::Context>& cr);
    bool on_button_press_event(GdkEventButton *event);
    bool on_key_press_event(GdkEventKey *event);
    void on_monitor_added(const Glib::RefPtr<Gdk::Monitor>& mon);
    void redraw();
};

GuiTester::GuiTester(Gtk::Main *papp)
 : app(papp)
{
    first_time = true;
    add_events(Gdk::KEY_PRESS_MASK | Gdk::KEY_RELEASE_MASK | Gdk::BUTTON_PRESS_MASK | Gdk::BUTTON_RELEASE_MASK);
    displayRef->signal_monitor_removed().connect(sigc::mem_fun(*this, &GuiTester::on_monitor_added), false);
    displayRef->signal_monitor_added().connect(sigc::mem_fun(*this, &GuiTester::on_monitor_added), false);

    curr_x = get_width() / 2;
    curr_y = get_height() / 2;
    set_can_focus(true);
}

void GuiTester::on_monitor_added(const Glib::RefPtr<Gdk::Monitor>& mon) {
    int n_monitors = screen->get_n_monitors();
    monitornum = monnum;
    if (monnum >= n_monitors) monitornum = n_monitors-1;
    Gdk::Rectangle destRect;
    screen->get_monitor_geometry(monitornum, destRect);
//    fprintf(stderr, "Monitors count: %u, move to %d, %d\n", n_monitors, destRect.get_x(), destRect.get_y());
    mainwin->move(destRect.get_x(), destRect.get_y());
    mainwin->fullscreen();
    redraw();
}

bool GuiTester::on_draw(const Cairo::RefPtr<Cairo::Context>& cr) {
    cr->save();
    double display_width  = get_width();
    double display_height = get_height();

    int win_x, win_y;

    mainwin->get_position(win_x, win_y);

    cr->set_source_rgb(1.0, 1.0, 1.0);
    cr->rectangle(0, 0, get_width(), get_height());
    cr->fill();

    cr->set_source_rgb(0.0, 0.0, 0.0);
    cr->set_line_width(2);
    cr->rectangle(display_width/40, display_width/40, display_width-(display_width/20), display_height-(display_width/20));
    cr->stroke();

    int radius = (display_height-(display_width/20))/2;
    if (main_screen) {
        cr->set_source_rgb(1.0, 0.8, 0.9);
        cr->arc(display_width/2, display_height/2, radius, 0, 2 * M_PI);
        cr->fill();
    }
    cr->set_source_rgb(0.0, 0.0, 0.0);
    cr->arc(display_width/2, display_height/2, radius, 0, 2 * M_PI);
    cr->stroke();

    char strcoord[32], buffer[64];
    sprintf(strcoord, "X:%0.0f, Y:%0.0f", curr_x, curr_y);
    cr->set_font_size(32);
    cr->select_font_face ("Bitstream Vera Sans",
        Cairo::FontSlant::FONT_SLANT_NORMAL,
        Cairo::FontWeight::FONT_WEIGHT_BOLD);
    Cairo::TextExtents extent;

    cr->get_text_extents(strcoord, extent);
    double text_width = extent.width;
    double text_height = extent.height;
    double x = (display_width - extent.width) / 2;
    double y = (display_height - extent.height) / 2;

    cr->set_source_rgb(1.0, 0.0, 0.0);
    sprintf(buffer, "Экран № %0d (%0.0f*%0.0f)", monitornum+1, display_width, display_height);

    cr->get_text_extents(buffer, extent);
    cr->rectangle(display_width/40, display_width/40, extent.width+20, extent.height+20);
    cr->fill();

    cr->set_source_rgb(1.0, 1.0, 0.0);
    cr->move_to(display_width/40 + 10, display_width/40 + extent.height + 10);

    cr->show_text(buffer);

    if (!first_time) {
	cr->set_source_rgb(0.5, 0.5, 0.5);

	cr->move_to(x, y);
	cr->show_text(strcoord);
	cr->stroke();
    }
    if (win_x > 1 || win_y > 1) {
	Glib::RefPtr<Gdk::Pixbuf> pixbuf = Gdk::Pixbuf::create_from_file(win_x > 1 ? "/usr/share/tester/arrowleft.png" : "/usr/share/tester/arrowup.png");
	double img_width  = pixbuf->get_width();
	double img_height = pixbuf->get_height();

	double imgx = (win_x > 1 ? 0 : (display_width - img_width) / 2);
	double imgy = (win_y > 1 ? 0 : (display_height - img_height) / 2);

	Gdk::Cairo::set_source_pixbuf(cr, pixbuf, imgx, imgy );
	cr->rectangle(imgx, imgy, img_width, img_height);
	cr->fill();

	cr->set_font_size(25);
	sprintf(strcoord, "%u", (win_x > 1 ? win_x : win_y));
	cr->get_text_extents(strcoord, extent);
	text_width = extent.width;
	text_height = extent.height;

	x = imgx + ((img_width - (win_x > 1 ? extent.width : extent.height)) / 2);
	y = imgy + ((img_height - (win_x > 1 ? extent.height : extent.width)) / 2);
	cr->set_source_rgb(1.0, 1.0, 0.0);
	cr->move_to(x, y + (win_x > 1 ? text_height : 0));
	if (win_y > 0)
	    cr->rotate(M_PI / 2);
	cr->show_text(strcoord);
	cr->stroke();
	cr->restore();
	cr->save();
    }
    if (!first_time) {
	cr->set_source_rgb(1.0, 0.0, 0.0);
	cr->arc(curr_x, curr_y, 10, 0, 2 * M_PI);
	cr->fill();
	cr->set_line_width(1);
	cr->arc(curr_x, curr_y, 15, 0, 2 * M_PI);
	cr->arc(curr_x, curr_y, 20, 0, 2 * M_PI);
	cr->stroke();
	cr->set_source_rgb(0.0, 0.0, 0.7);
	cr->set_line_width(1);
	std::valarray<double> dashes(2);
	dashes[0] = 2.0;
	dashes[1] = 2.0;
	cr->set_dash (dashes, 0.0);
	cr->move_to(curr_x, 0);
	cr->line_to(curr_x, display_height);
	cr->move_to(0, curr_y);
	cr->line_to(display_width, curr_y);
	cr->stroke();
    }
    cr->restore();

    return true;
}

void GuiTester::redraw() {
    Glib::RefPtr<Gdk::Window> win = get_window();
    if (win) {
        const Gdk::Rectangle rect(0, 0, get_width(), get_height());
        win->invalidate_rect(rect, false);
    }
}

bool GuiTester::on_button_press_event(GdkEventButton *event) {
    curr_x = event->x;
    curr_y = event->y;
    first_time = false;
    redraw();
    return true;
}

bool GuiTester::on_key_press_event(GdkEventKey *event) {
    (void) event;
    app->quit();
    return true;
}

static void usage(char* cmd) {
    fprintf(stderr, "Usage: %s [-h|--help] [-m|--monitor <number>] [-f|--first]\n", cmd);
    fprintf(stderr, "\t-h, --help: print this help message\n");
    fprintf(stderr, "\t-m, --monitor: using given monitor number for output\n");
    fprintf(stderr, "\t-f, --first: show main application on this screen\n");
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {

    const char* short_opts = "hm:f";
    static struct option long_opts[] = {
                    { "help", no_argument, 0, 'h'},
                    { "monitor", required_argument, 0, 'm'},
                    { "first", no_argument, 0, 'f'},
                    { 0, 0, 0, 0 }
    };
    int option_index = (-1), ch;
    while ((ch = getopt_long(argc, argv, short_opts, long_opts, &option_index)) != -1) {
       switch (ch) {
         case 'm':
             if (optarg)
                monnum = atoi(optarg); 
             else {
                fprintf(stderr, "Required argument: monitor number.\n");
                usage(argv[0]);
             }
             break;
         case 'f':
             main_screen = true;
             break;
         case 'h':
             usage(argv[0]);
             break;
         case '?': default:
             usage(argv[0]);
             break;
       }
       option_index = (-1);
    }
    Gtk::Main app(argc, argv);
    Gtk::ApplicationWindow win;
    mainwin = &win;
    screen = win.get_screen();
    int n_monitors = screen->get_n_monitors();
    monitornum = monnum;
    if (monnum >= n_monitors) monitornum = n_monitors-1;
    Gdk::Rectangle destRect;
    displayRef = screen->get_display();

    screen->get_monitor_geometry(monitornum, destRect);
    GuiTester area(&app);
    win.add(area);
    area.show();
    win.move(destRect.get_x(), destRect.get_y());
    win.fullscreen();
    app.run(win);
    return EXIT_SUCCESS;
}
